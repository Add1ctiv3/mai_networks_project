package uom.mai.masterproject1.operations;

import android.content.Intent;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.android.gms.common.AccountPicker;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import uom.mai.masterproject1.R;
import uom.mai.masterproject1.activities.HomeActivity;
import uom.mai.masterproject1.activities.TaskActivity;
import uom.mai.masterproject1.async_tasks.ResultPacket;
import uom.mai.masterproject1.async_tasks.TasksAsyncTask;
import uom.mai.masterproject1.database.Task;
import uom.mai.masterproject1.ui.TaskInflater;
import static android.content.ContentValues.TAG;

public class HomeActivityOps extends MyOpsType {

    public static final int CompletedTasksCode = 111;
    public static final int OverdueTasksCode = 222;
    public static final int FutureTasksCode = 333;
    public static final int todayTasksCode = 444;
    public static final int allTasksCode = 555;

    private WeakReference<HomeActivity> mActivity;
    public HomeActivity getActivity() { return mActivity.get(); }
    public void updateActivityRef(HomeActivity activity) { mActivity = new WeakReference<>(activity); }

    private ArrayList<Task> mTasks;
    public void setTasks(ArrayList<Task> tasks) { mTasks = tasks; }
    public ArrayList<?> getTasks() { return mTasks; }

    private TasksAsyncTask mAsyncTask;

    private LinearLayout mainView;

    private int mWindowCode = 444;
    public void setWindowCode(int i) { mWindowCode = i; }
    public int getWindowCode() { return mWindowCode; }

    public HomeActivityOps(HomeActivity activity) {

        mActivity = new WeakReference<>(activity);

        checkForUserId();

        loadTodayTasks();

    }

    private void checkForUserId() {

        String id = StaticOps.loadPreferences(mActivity.get());
        if(id.equals("")) {
            Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{"com.google"}, false, null, null, null, null);
            mActivity.get().startActivityForResult(intent, 1111);
        } else {
            StaticOps.ID_OK = true;
            StaticOps.USER_ID = id;
        }

    }



    //function that loads all the UI elements
    private void setSecondaryUI(TasksAsyncTask.Operation mode) {

        mainView = (LinearLayout) mActivity.get().findViewById(R.id.homeActivityMainScrollView);

        TaskInflater inflater = new TaskInflater(mActivity.get(), mainView, mTasks, mode);

        inflater.inflate();

    }

    public void loadOverdueTasks() {

        if(mainView != null) {
            mainView.removeAllViews();
        }
        mAsyncTask = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.LOAD_OVERDUE);
        mAsyncTask.execute();

    }

    public void loadAllTasks() {

        if(mainView != null) {
            mainView.removeAllViews();
        }
        mAsyncTask = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.LOAD_ALL);
        mAsyncTask.execute();

    }

    public void loadFutureTasks() {

        if(mainView != null) {
            mainView.removeAllViews();
        }
        mAsyncTask = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.LOAD_FUTURE);
        mAsyncTask.execute();

    }

    //function that loads all tasks from the local database
    public void loadTodayTasks() {

        if(mainView != null) {
            mainView.removeAllViews();
        }
        mAsyncTask = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.LOAD_TODAYS);
        mAsyncTask.execute();

    }

    public void loadCompletedTasks() {

        if(mainView != null) {
            mainView.removeAllViews();
        }
        mAsyncTask = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.LOAD_COMPLETED);
        mAsyncTask.execute();

    }

    public void onCreateNewTaskButtonClick() {
        Intent intent = new Intent(getActivity(), TaskActivity.class);
        mActivity.get().startActivityForResult(intent, mWindowCode);
    }

    /**
     * Called by the HomeActivity activity and after a runtime
     * configuration change occurs to finish the initialization steps.
     */
    public void onConfigurationChange(HomeActivity activity) {

        // Reset the mActivity WeakReference.
        mActivity = new WeakReference<>(activity);

        if(mAsyncTask != null) {
            mAsyncTask.updateActivityRef(activity);
        }

        switch(mWindowCode) {
            case 111:
                loadCompletedTasks();
                break;
            case 222:
                loadOverdueTasks();
                break;
            case 333:
                loadFutureTasks();
                break;
            case 444:
                loadTodayTasks();
                break;
            case 555:
                loadAllTasks();
                break;
        }

    }

    public void onActivityResult(int code) {

        switch(code) {
            case 111:
                loadCompletedTasks();
                break;
            case 222:
                loadOverdueTasks();
                break;
            case 333:
                loadFutureTasks();
                break;
            case 444:
                loadTodayTasks();
                break;
            case 555:
                loadAllTasks();
                break;
        }

    }

    @Override
    public void onTaskAsyncTaskPostExecute(ResultPacket result) {

        setTasks(result.getTasks());

        if(result.getMode().compareTo(TasksAsyncTask.Operation.DRAG_UPDATE) == 0) {
            Toast.makeText(mActivity.get(), "Επιτυχής αλλαγή ημερομηνίας!", Toast.LENGTH_LONG).show();
            loadTodayTasks();
        }
         else  {
            setSecondaryUI(result.getMode());
        }

    }
}
