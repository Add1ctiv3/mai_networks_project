package uom.mai.masterproject1.operations;

import android.content.Context;
import android.content.SharedPreferences;

public class StaticOps {

    private static final String KEY = "mai17037.uom.project.preferences_key";

    public static boolean ID_OK = false;
    public static String USER_ID = "";

    public static void savePreferences(Context context, String userEmail) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                StaticOps.KEY, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString("USER_ID", userEmail);
        editor.commit();

    }

    public static String loadPreferences(Context context) {

        SharedPreferences shp = context.getSharedPreferences(StaticOps.KEY, context.MODE_PRIVATE);

        String id = shp.getString("USER_ID", "");
        if(id == null || id.equals("")) {
            return "";
        }

        return id;

    }

    public static void saveLastSyncDate(Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                StaticOps.KEY, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putInt("LAST_SYNC", (int)System.currentTimeMillis()/1000);
        editor.commit();

    }

    public static int loadLastSyncDate(Context context) {

        SharedPreferences shp = context.getSharedPreferences(StaticOps.KEY, context.MODE_PRIVATE);

        return shp.getInt("LAST_SYNC", 0);

    }




}
