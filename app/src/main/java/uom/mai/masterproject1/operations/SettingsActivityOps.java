package uom.mai.masterproject1.operations;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import uom.mai.masterproject1.R;
import uom.mai.masterproject1.activities.SettingsActivity;
import uom.mai.masterproject1.async_tasks.EndpointsAsyncTask;
import uom.mai.masterproject1.async_tasks.ResultPacket;
import uom.mai.masterproject1.async_tasks.TasksAsyncTask;
import uom.mai.masterproject1.database.Task;

public class SettingsActivityOps extends MyOpsType {

    private WeakReference<SettingsActivity> mActivity;
    public SettingsActivity getActivity() { return mActivity.get(); }
    public void updateActivityRef(SettingsActivity activity) { mActivity = new WeakReference<SettingsActivity>(activity); }

    private TextView userIdView;
    private Button mSyncButton;

    public SettingsActivityOps(SettingsActivity activity) {

        mActivity = new WeakReference<SettingsActivity>(activity);

        setUI();
        loadUserId();

    }

    public void setUI() {

        mActivity.get().setContentView(R.layout.activity_settings);

        userIdView = (TextView) mActivity.get().findViewById(R.id.userID);
        mSyncButton = (Button) mActivity.get().findViewById(R.id.syncButton);

        mSyncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EndpointsAsyncTask async = new EndpointsAsyncTask(mActivity.get());
                async.execute();
            //TasksAsyncTask async = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.REMOTE, TasksAsyncTask.Operation.SYNC);
            //async.execute();

            }
        });

    }

    public void loadUserId() {

        String id = StaticOps.loadPreferences(mActivity.get());
        userIdView.setText(id);

    }

    public void onEndpointAsyncTaskPostExecute(ArrayList<Task>  tasks) {


        TasksAsyncTask async = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.BULK_UPDATE);
        async.setTaskList(tasks);
        async.execute();


    }

    @Override
    public void onTaskAsyncTaskPostExecute(ResultPacket result) {
        super.onTaskAsyncTaskPostExecute(result);
        Toast.makeText(mActivity.get(), "Ο συγχρονισμός ολοκληρώθηκε επιτυχώς!", Toast.LENGTH_LONG).show();
    }

    /**
     * Called by the SettingsActivity activity and after a runtime
     * configuration change occurs to finish the initialization steps.
     */
    public void onConfigurationChange(SettingsActivity activity) {

        // Reset the mActivity WeakReference.
        mActivity = new WeakReference<>(activity);

        setUI();

    }

}
