package uom.mai.masterproject1.operations;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import uom.mai.masterproject1.R;
import uom.mai.masterproject1.activities.TaskActivity;
import uom.mai.masterproject1.async_tasks.ResultPacket;
import uom.mai.masterproject1.async_tasks.TasksAsyncTask;
import uom.mai.masterproject1.database.Task;

public class TaskActivityOps extends MyOpsType {

    private ImageButton mCloseButton, mDateButton, mTimeButton, mAlertButton, mDeleteButton;
    private FloatingActionButton mCreateUpdateButton;
    private EditText mNoteView;
    private TextView mDateView;

    private Task mTask;
    private boolean mAlert;

    private TasksAsyncTask mAsyncTask;
    private Dialog mDateDialog;
    private Dialog mTimeDialog;

    private String initialDate;


    private WeakReference<TaskActivity> mActivity;
    public TaskActivity getActivity() { return mActivity.get(); }
    public void updateActivityRef(TaskActivity activity) { mActivity = new WeakReference<TaskActivity>(activity); }

    public TaskActivityOps(TaskActivity activity) {

        mActivity = new WeakReference<TaskActivity>(activity);

        setSecondaryUI();
        setTriggers();

    }

    //function that loads all the UI elements
    private void setSecondaryUI() {

        Intent intent = mActivity.get().getIntent();
        mTask = intent.getParcelableExtra("TASK_EXTRA");

        mCloseButton = (ImageButton) mActivity.get().findViewById(R.id.closeButton);
        mDateButton = (ImageButton) mActivity.get().findViewById(R.id.date_button);
        mTimeButton = (ImageButton) mActivity.get().findViewById(R.id.time_button);
        mTimeButton.setActivated(false);
        mAlertButton = (ImageButton) mActivity.get().findViewById(R.id.alert_toggle_button);
        mCreateUpdateButton = (FloatingActionButton) mActivity.get().findViewById(R.id.createUpdateTaskButton);
        mNoteView = (EditText) mActivity.get().findViewById(R.id.note);
        mDateView = (TextView) mActivity.get().findViewById(R.id.dateView);
        mDeleteButton = (ImageButton) mActivity.get().findViewById(R.id.deleteButton);

        if(mTask == null) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            mTask = new Task(sdf.format(calendar.getTime()), "");
            mDeleteButton.setVisibility(View.INVISIBLE);
        } else {
            initialDate = mTask.getDate();
        }

        mAlert = false;

        if(mTask != null) {
            mDateView.setText(mTask.getDate());
            mNoteView.setText(mTask.getNote());
            if(mTask.doNotify()) {
                mAlert = true;
                mAlertButton.setImageResource(R.mipmap.ic_alarm_on_white_24dp);
                mAlertButton.setBackgroundResource(R.drawable.rounded_corners_shape_green);
                mTimeButton.setActivated(true);
                mTimeButton.setBackgroundResource(R.drawable.rounded_corners_shape);
            }
        }

    }

    private void setTriggers() {

        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.get().finish();
            }
        });

        mCreateUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            mTask.setNote(mNoteView.getText().toString());
            mTask.setDate(mDateView.getText().toString());
            mTask.setNotify(mAlert);

            if(mTask != null && mTask.getDate() != null && !mTask.getDate().equals("") && !mTask.getNote().equals("")) {

                saveUpdateTask();

            } else {
                Toast.makeText(mActivity.get(), "Συμπληρώστε τουλάχιστον ημερομηνία και μήνυμα εκκρεμότητας!", Toast.LENGTH_LONG).show();
            }

            }
        });

        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AlertDialog.Builder(mActivity.get())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Διαγραφή Εκκρεμότητας;")
                    .setMessage("Είστε σίγουρος/η ότι θέλετε να διαγράψετε αυτήν την εκκρεμότητα;")
                    .setPositiveButton("Ναι", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            TasksAsyncTask task = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.DELETE);
                            task.execute(mTask);
                            dialog.dismiss();

                        }

                    })
                    .setNegativeButton("Οχι", null)
                    .show();

            }
        });

        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.get().showDialog(999);
            }
        });

        mAlertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAlert = !mAlert;
                if(mAlert) {
                    mAlertButton.setBackgroundResource(R.mipmap.ic_alarm_on_white_24dp);
                    mAlertButton.setBackgroundResource(R.drawable.rounded_corners_shape_green);
                    mTimeButton.setBackgroundResource(R.drawable.rounded_corners_shape);
                    mTimeButton.setActivated(true);
                } else {
                    if(mTask != null && mTask.getTime() != null) { mTask.setTime(null); }
                    mAlertButton.setBackgroundResource(R.mipmap.ic_alarm_off_white_24dp);
                    mAlertButton.setBackgroundResource(R.drawable.rounded_corners_shape_red);
                    mTimeButton.setBackgroundResource(R.drawable.rounded_corners_shape_grey);
                    mTimeButton.setActivated(false);
                }
            }
        });

        mTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mTimeButton.isActivated()) {
                    mActivity.get().showDialog(888);
                }
            }
        });

    }

    public Dialog onCreateDateDialog() {
        if(mTask != null && mTask.getDate() != null) {
            String[] dateParts = mTask.getDate().split("/");
            return new DatePickerDialog(mActivity.get(), myDateListener, Integer.parseInt(dateParts[2]), (Integer.parseInt(dateParts[1])-1), Integer.parseInt(dateParts[0]));
        }
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        mDateDialog = new DatePickerDialog(mActivity.get(), myDateListener, date.getYear(), date.getMonth(), date.getDay()+1);
        return mDateDialog;
    }

    public Dialog onCreateTimeDialog() {
        if(mTask != null && mTask.getTime() != null) {
            String[] timeParts = mTask.getTime().split(":");
            Log.d("TEST", timeParts[0]);
            return new TimePickerDialog(mActivity.get(), myTimeListener, Integer.parseInt(timeParts[0]), Integer.parseInt(timeParts[1]), true);
        }
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        mTimeDialog = new TimePickerDialog(mActivity.get(), myTimeListener, date.getHours(), date.getMinutes(), true);
        return mTimeDialog;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            String sdate = "";
            if(arg3 < 10) {
                sdate += "0" + arg3;
            } else {
                sdate += arg3;
            }

            sdate += "/";

            if((arg2+1) < 10) {
                sdate += "0" + (arg2+1);
            } else {
                sdate += (arg2+1);
            }

            sdate += "/";

            sdate += arg1;

            mTask.setDate(sdate);
            mDateView.setText(sdate);
        }
    };

    private TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker arg0, int arg1, int arg2) {

            String timeS = "";

            if(arg1 < 10) {
                timeS += "0" + arg1;
            }else {
                timeS += arg1;
            }

            timeS += ":";

            if(arg2 < 10) {
                timeS += "0" + arg2;
            }else {
                timeS += arg2;
            }

            mTask.setTime(timeS);
        }
    };

    private void saveUpdateTask() {

        if(initialDate != null && !mTask.getDate().equals(initialDate)) {
            mAsyncTask = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.UPDATE);
            mAsyncTask.execute(mTask);
            return;
        }

        mAsyncTask = new TasksAsyncTask(mActivity.get(), TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.SAVE_UPDATE);
        mAsyncTask.execute(mTask);

    }

    @Override
    public void onTaskAsyncTaskPostExecute(ResultPacket result) {

        Intent returnIntent = new Intent();
        mActivity.get().setResult(Activity.RESULT_OK, returnIntent);
        mActivity.get().finish();

    }

    /**
     * Called by the TaskActivity activity and after a runtime
     * configuration change occurs to finish the initialization steps.
     */
    public void onConfigurationChange(TaskActivity activity) {

        // Reset the mActivity WeakReference.
        mActivity = new WeakReference<>(activity);

        setSecondaryUI();

    }

}
