package uom.mai.masterproject1.ui;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;

import uom.mai.masterproject1.R;
import uom.mai.masterproject1.database.DatabaseOps;
import uom.mai.masterproject1.database.Task;

public final class MyTouchListener implements View.OnLongClickListener {

    @Override
    public boolean onLongClick(View view) {

        ClipData data = ClipData.newPlainText("", "");

        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                view);

        view.startDrag(data, shadowBuilder, view, 0);

        return true;
    }

}
