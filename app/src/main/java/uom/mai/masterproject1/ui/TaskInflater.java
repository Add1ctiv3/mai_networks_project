package uom.mai.masterproject1.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import uom.mai.masterproject1.R;
import uom.mai.masterproject1.activities.MyActivityType;
import uom.mai.masterproject1.activities.TaskActivity;
import uom.mai.masterproject1.async_tasks.TasksAsyncTask;
import uom.mai.masterproject1.database.Task;
import uom.mai.masterproject1.operations.HomeActivityOps;
import static uom.mai.masterproject1.ui.MyDragListener.TimeState.NON_OVERDUE;
import static uom.mai.masterproject1.ui.MyDragListener.TimeState.OVERDUE;

public class TaskInflater {

    private MyActivityType mActivity;
    private ViewGroup mFinalContainer;
    private ArrayList<Task> mTasks;

    private TasksAsyncTask.Operation mMode;

    public TaskInflater(Activity activity, ViewGroup whereToInflate, ArrayList<Task> tasks, TasksAsyncTask.Operation mode) {

        mActivity = (MyActivityType) activity;
        mFinalContainer = whereToInflate;
        mMode = mode;
        mTasks = tasks;

    }

    public void inflate() {

        switch(mMode) {
            case LOAD_TODAYS:
                inflateTodayTasks();
                break;
            case LOAD_OVERDUE:
                inflateOverdueTasks();
                break;
            case LOAD_FUTURE:
                inflateFutureTasks();
                break;
            case LOAD_ALL:
                inflateAllTasks();
                break;
            case LOAD_COMPLETED:
                inflateCompletedTasks();
                break;
        }

    }

    private void inflateAllTasks() {

        if(mTasks == null) { return; }

        ArrayList<Task> today = new ArrayList<>();
        ArrayList<Task> overdue = new ArrayList<>();
        ArrayList<Task> future = new ArrayList<>();

        String date = getTodayDate();
        Calendar calendar = Calendar.getInstance();
        Date nowDate = calendar.getTime();

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf =new SimpleDateFormat("dd/MM/yyyy");

        for(Task task: mTasks) {

            try {

                Date taskDate = sdf.parse(task.getDate());

                if(task.getDate().equals(date)) {
                    today.add(task);
                    continue;
                }
                if(taskDate.after(nowDate)) {
                    future.add(task);
                    continue;
                }
                if(taskDate.before(nowDate)) {
                    overdue.add(task);
                }

            } catch (ParseException e) {
                return;
            }

        }

        if(overdue.size() != 0) {

            LinearLayout overdueContainer = getTaskCategoryLayout("Εκπρόθεσμες", overdue, false);

            overdueContainer.setOnDragListener(new MyDragListener(mActivity, OVERDUE));

            mFinalContainer.addView(overdueContainer);

        }

        if(today.size() != 0) {

            LinearLayout todayContainer = getTaskCategoryLayout("Σημερινές", today, true);

            todayContainer.setOnDragListener(new MyDragListener(mActivity, NON_OVERDUE));

            mFinalContainer.addView(todayContainer);

        }

        if(future.size() != 0) {

            LinearLayout futureContainer = getTaskCategoryLayout("Μελλοντικές", future, false);

            futureContainer.setOnDragListener(new MyDragListener(mActivity, NON_OVERDUE));

            mFinalContainer.addView(futureContainer);

        }

    }

    private void inflateCompletedTasks() {

        if(mTasks!=null) {
            LinearLayout completedContainer = getTaskCategoryLayout("Ολοκληρωμένες", mTasks, false);
            mFinalContainer.addView(completedContainer);
        } else {
            LinearLayout completedContainer = getTaskCategoryLayout("Ολοκληρωμένες", new ArrayList<Task>(), false);
            mFinalContainer.addView(completedContainer);
        }

    }

    private void inflateTodayTasks() {

        if(mTasks == null) { return; }

        ArrayList<Task> today = new ArrayList<>();
        ArrayList<Task> overdue = new ArrayList<>();

        String date = getTodayDate();

        for(Task task: mTasks) {

            if(task.getDate().equals(date)) {
                today.add(task);
                continue;
            }

            overdue.add(task);

        }

        if(overdue.size() != 0) {

            LinearLayout overdueContainer = getTaskCategoryLayout("Εκπρόθεσμες", overdue, false);

            overdueContainer.setOnDragListener(new MyDragListener(mActivity, OVERDUE));

            mFinalContainer.addView(overdueContainer);

        }

        LinearLayout todayContainer = getTaskCategoryLayout("Σημερινές", today, true);

        todayContainer.setOnDragListener(new MyDragListener(mActivity, NON_OVERDUE));

        mFinalContainer.addView(todayContainer);

        if(mMode.compareTo(TasksAsyncTask.Operation.DRAG_UPDATE) == 0) {
            Toast.makeText(mActivity, "Επιτυχής αλλαγή ημερομηνίας!", Toast.LENGTH_LONG).show();
        }

    }

    private void inflateOverdueTasks() {

        if(mTasks == null) { return; }

        if(mTasks.size() != 0) {

            LinearLayout overdueContainer = getTaskCategoryLayout("Εκπρόθεσμες", mTasks, false);

            overdueContainer.setOnDragListener(new MyDragListener(mActivity, OVERDUE));

            mFinalContainer.addView(overdueContainer);

        }

    }

    private void inflateFutureTasks() {

        if(mTasks == null) { return; }

        if(mTasks.size() != 0) {

            LinearLayout overdueContainer = getTaskCategoryLayout("Μελλοντικές", mTasks, false);

            overdueContainer.setOnDragListener(new MyDragListener(mActivity, NON_OVERDUE));

            mFinalContainer.addView(overdueContainer);

        }

    }

    private LinearLayout getTaskCategoryLayout(String title, ArrayList<Task> list, boolean showDate) {

        LayoutInflater inflater = LayoutInflater.from(mActivity);

        LinearLayout container = (LinearLayout) inflater.inflate(R.layout.task_category_container, null, false);

        TextView dayLabelView = (TextView) container.findViewById(R.id.dayLabel);
        TextView dateLabelView = (TextView) container.findViewById(R.id.datelabel);

        dayLabelView.setText(title);

        if(showDate) {
            dateLabelView.setText(getTodayDate());
        } else {
            if(title.equals("Εκπρόθεσμες")) {
                dayLabelView.setTextColor(Color.RED);
            }
        }

        getTasksLayout(container, list);

        return container;

    }

    private LinearLayout getTasksLayout(LinearLayout container, ArrayList<Task> list) {

        final LinearLayout load = (LinearLayout) container.findViewById(R.id.tasksContainer);

        LayoutInflater inflater = LayoutInflater.from(mActivity);

        for(Task task: list) {

            final RelativeLayout taskLayout = (RelativeLayout) inflater.inflate(R.layout.task_item, null, false);

            TextView noteView = (TextView) taskLayout.findViewById(R.id.taskNoteField);
            TextView dateView = (TextView) taskLayout.findViewById(R.id.taskDateField);
            TextView timeView = (TextView) taskLayout.findViewById(R.id.taskNotifyTimeField);
            final ImageButton checkButton = (ImageButton) taskLayout.findViewById(R.id.completeButton);

            noteView.setText(task.getNote());
            dateView.setText(task.getDate());

            if(task.getTime() != null) {
                timeView.setText(task.getTime());
            }

            final Task mytask = task;

            if(mytask.isCompleted()) {
                checkButton.setVisibility(View.INVISIBLE);
            }

            //OnClickListener
            taskLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(mActivity, TaskActivity.class);
                    intent.putExtra("TASK_EXTRA", mytask);

                    HomeActivityOps ops = (HomeActivityOps) mActivity.getOps();
                    int code = ops.getWindowCode();

                    mActivity.startActivityForResult(intent, code);

                }
            });

            checkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mytask.setCompleted(true);

                    final View check = view;

                    int colorFrom = Color.WHITE;
                    int colorTo = mActivity.getResources().getColor(R.color.completedTaskGreen);
                    ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
                    colorAnimation.setDuration(250); // milliseconds
                    colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                        @Override
                        public void onAnimationUpdate(ValueAnimator animator) {
                            taskLayout.setBackgroundColor((int) animator.getAnimatedValue());
                        }

                    });
                    colorAnimation.start();

                    colorAnimation.addListener(new AnimatorListenerAdapter()
                    {
                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            check.setVisibility(View.INVISIBLE);

                            ViewGroup main = ((ViewGroup)taskLayout.getParent());

                            ((ViewGroup)taskLayout.getParent()).removeView(taskLayout);
                            if(main.getChildCount() == 0) {
                                ((ViewGroup)main.getParent()).removeAllViews();
                            }

                            TasksAsyncTask async = new TasksAsyncTask(mActivity, TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.COMPLETE_TASK);
                            async.execute(mytask);

                        }
                    });

                }
            });

            //OnLongClickListener
            taskLayout.setOnLongClickListener(new MyTouchListener());

            load.addView(taskLayout);

        }

        return load;

    }

    private String getTodayDate() {
        Calendar cal = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(cal.getTime());
    }

}
