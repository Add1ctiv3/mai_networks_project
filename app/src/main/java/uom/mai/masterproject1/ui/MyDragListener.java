package uom.mai.masterproject1.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;

import uom.mai.masterproject1.R;
import uom.mai.masterproject1.activities.MyActivityType;
import uom.mai.masterproject1.async_tasks.TasksAsyncTask;
import uom.mai.masterproject1.database.DatabaseOps;
import uom.mai.masterproject1.database.Task;

public class MyDragListener implements View.OnDragListener {

    public enum TimeState { OVERDUE, NON_OVERDUE }

    private MyActivityType mContext;

    Drawable enterShape;
    Drawable normalShape;

    private TimeState mState;

    public MyDragListener(MyActivityType context, TimeState state) {
        mContext = context;
        enterShape = context.getResources().getDrawable(R.drawable.shape_droptarget);
        normalShape = context.getResources().getDrawable(R.drawable.shape);
        mState = state;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        int action = event.getAction();
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                //do nothing
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                v.setBackgroundDrawable(enterShape);
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                v.setBackgroundDrawable(normalShape);
                break;
            case DragEvent.ACTION_DROP:
                if(mState.compareTo(TimeState.NON_OVERDUE) == 0) {
                    // Dropped, reassign View to ViewGroup
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view);
                    LinearLayout container = (LinearLayout) v;
                    container.addView(view);
                    view.setVisibility(View.VISIBLE);

                    String note;
                    String date;

                    TextView noteView = (TextView) view.findViewById(R.id.taskNoteField);
                    note = noteView.getText().toString();

                    TextView dateView = (TextView) view.findViewById(R.id.taskDateField);
                    date = dateView.getText().toString();

                    updateTask(note, date);

                    return true;
                }
                return false;

            case DragEvent.ACTION_DRAG_ENDED:
                v.setBackgroundDrawable(normalShape);
                if(!event.getResult()) {
                    v.setVisibility(View.VISIBLE);
                }
                return true;
            default:
                break;
        }
        return true;
    }

    private void updateTask(String note, String date) {

        TasksAsyncTask async = new TasksAsyncTask(mContext, TasksAsyncTask.Mode.LOCAL, TasksAsyncTask.Operation.DRAG_UPDATE);
        async.execute(new Task(date, note));

    }
}