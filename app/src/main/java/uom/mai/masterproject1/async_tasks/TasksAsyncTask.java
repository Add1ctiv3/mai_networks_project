package uom.mai.masterproject1.async_tasks;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.widget.Toast;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import addictivelabs.mai.uom.taskEndpoint.TaskEndpoint;
import uom.mai.masterproject1.activities.MyActivityType;
import uom.mai.masterproject1.database.DatabaseOps;
import uom.mai.masterproject1.database.Task;

public class TasksAsyncTask extends AsyncTask<Task, Integer, ResultPacket> {

    public enum Mode { LOCAL, REMOTE }
    public enum Operation { SAVE_UPDATE, DELETE, LOAD_TODAYS, LOAD_FUTURE, UPDATE, LOAD_OVERDUE, LOAD_COMPLETED, LOAD_ALL, COMPLETE_TASK, DRAG_UPDATE, SYNC, BULK_UPDATE }

    private WeakReference<MyActivityType> mActivity;
    private Mode mMode;
    private Operation mOperation;

    private ArrayList<Task> mTasks;
    public void setTaskList(ArrayList<Task> tasks) { mTasks = tasks; }

    private static TaskEndpoint myApiService = null;

    public void updateActivityRef(MyActivityType activity) { mActivity = new WeakReference<>(activity); }

    public TasksAsyncTask(MyActivityType activity, Mode mode, Operation operation) {

        mMode = mode;
        mOperation = operation;
        mActivity = new WeakReference<>(activity);

    }

    @Override
    protected ResultPacket doInBackground(Task... tasks) {

        if(mMode.compareTo(Mode.LOCAL) == 0) {

            Task task;

            if(tasks.length > 0) {
              task   = tasks[0];
            } else {
                task = null;
            }

            switch(mOperation) {

                case LOAD_TODAYS:
                    return new ResultPacket(DatabaseOps.getTodayTasks(mActivity.get()), mOperation);

                case SAVE_UPDATE:

                    if(task != null) {
                        task.setNeedsSync(true);
                        if(!DatabaseOps.taskWithThisIdExists(task, mActivity.get()) && !DatabaseOps.taskExists(task, mActivity.get())) {
                            DatabaseOps.insertTask(task, mActivity.get());
                        } else {
                            DatabaseOps.updateTask(tasks[0], mActivity.get());
                        }
                        return new ResultPacket(new ArrayList<Task>(), mOperation);
                    }

                case UPDATE:

                    if(task != null) {
                        task.setNeedsSync(true);
                        DatabaseOps.updateTask(task, mActivity.get());
                        return new ResultPacket(new ArrayList<Task>(), mOperation);
                    }

                case DELETE:

                    if(task != null) {
                        task.setNeedsSync(true);
                        task.setToBeDeleted(true);
                        DatabaseOps.updateTask(task, mActivity.get());
                        return new ResultPacket(new ArrayList<Task>(), mOperation);
                    }

                case LOAD_OVERDUE:

                    return new ResultPacket(DatabaseOps.getOverdueTasks(mActivity.get()), mOperation);

                case LOAD_FUTURE:

                    return new ResultPacket(DatabaseOps.getFutureTasks(mActivity.get()), mOperation);

                case LOAD_ALL:

                    return new ResultPacket(DatabaseOps.getAllTasks(mActivity.get()), mOperation);

                case LOAD_COMPLETED:

                    return new ResultPacket(DatabaseOps.getCompletedTasks(mActivity.get()), mOperation);

                case COMPLETE_TASK:

                    if (task != null) {
                        task.setNeedsSync(true);
                        DatabaseOps.updateTask(task, mActivity.get());
                        return new ResultPacket(new ArrayList<Task>(), mOperation);
                    }

                case DRAG_UPDATE:

                    try {
                        if(task != null) {
                            Task taskToUpdate = DatabaseOps.getTaskByNoteAndDate(task.getNote(), task.getDate(), mActivity.get());
                            if(taskToUpdate != null) {
                                Calendar calendar = Calendar.getInstance();
                                Date date = calendar.getTime();
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                taskToUpdate.setDate(sdf.format(date));
                                taskToUpdate.setNeedsSync(true);
                                DatabaseOps.updateTask(taskToUpdate, mActivity.get());
                                return new ResultPacket(new ArrayList<Task>(), mOperation);
                            }
                        }
                    } catch (ParseException e) {
                        Toast.makeText(mActivity.get(), "Ανεπιτυχής αλλαγή ημερομηνίας!", Toast.LENGTH_LONG).show();
                    }

                case BULK_UPDATE:

                    if(mTasks != null && mTasks.size() > 0) {

                        for(Task record: mTasks) {

                            if(DatabaseOps.getTask(record.getId(), mActivity.get()) != null) {

                                System.out.println("Task found in local database. Updating task with id " + record.getCalculatedId() + "...");
                                DatabaseOps.updateTask(record, mActivity.get());

                            } else {

                                System.out.println("Task NOT found in local database. Inserting task with id " + record.getCalculatedId() + "...");
                                DatabaseOps.insertTask(record, mActivity.get());

                            }

                        }

                    }

                    System.out.println("Deleting tasks that are marked to be deleted...");
                    DatabaseOps.deleteSyncedTasksToBeDeleted(mActivity.get());

                    return new ResultPacket(new ArrayList<Task>(), mOperation);

            }

        }

        return null;
    }

    @Override
    protected void onPostExecute(ResultPacket result) {
        super.onPostExecute(result);

        if(result.getMode().compareTo(Operation.DELETE) == 0) {
            Toast.makeText(mActivity.get(), "Η εκκρεμότητα διαγράφηκε επιτυχώς!", Toast.LENGTH_LONG).show();
        }

        mActivity.get().getOps().onTaskAsyncTaskPostExecute(result);

    }
}
