package uom.mai.masterproject1.async_tasks;


import java.util.ArrayList;

import uom.mai.masterproject1.database.Task;

public class ResultPacket {

    private ArrayList<Task> mTasks;
    private TasksAsyncTask.Operation mMode;

    public ArrayList<Task> getTasks() {
        return mTasks;
    }

    public TasksAsyncTask.Operation getMode() {
        return mMode;
    }

    public void setTasks(ArrayList<Task> mTasks) {
        this.mTasks = mTasks;
    }

    public void setMode(TasksAsyncTask.Operation mMode) {
        this.mMode = mMode;
    }

    public ResultPacket(ArrayList<Task> tasks, TasksAsyncTask.Operation mode) {

        mTasks = tasks;
        mMode = mode;

    }

}
