package uom.mai.masterproject1.async_tasks;

import android.os.AsyncTask;
import android.util.Log;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import java.io.IOException;
import java.util.ArrayList;
import addictivelabs.mai.uom.taskEndpoint.TaskEndpoint;
import addictivelabs.mai.uom.taskEndpoint.model.MyBean;
import addictivelabs.mai.uom.taskEndpoint.model.Task;
import uom.mai.masterproject1.activities.SettingsActivity;
import uom.mai.masterproject1.database.DatabaseOps;
import uom.mai.masterproject1.operations.StaticOps;

public class EndpointsAsyncTask extends AsyncTask<Void, Void, MyBean> {

    private static TaskEndpoint myApiService = null;
    private SettingsActivity context;

    public EndpointsAsyncTask(SettingsActivity context) {
        this.context = context;
    }

    @Override
    protected MyBean doInBackground(Void... params) {
        if(myApiService == null) {  // Only do this once
            TaskEndpoint.Builder builder = new TaskEndpoint.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    // options for running against local devappserver
                    // - 10.0.2.2 is localhost's IP address in Android emulator
                    // - turn off compression when running against local devappserver
                    //.setRootUrl("https://masterproject1-147520.appspot.com/_ah/api/")
                    .setRootUrl("http://10.0.2.2:8080/_ah/api/")
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                        @Override
                        public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                            abstractGoogleClientRequest.setDisableGZipContent(true);
                        }
                    });
            // end options for devappserver

            myApiService = builder.build();

        }

        MyBean bean = new MyBean();
        bean.setTasks(DatabaseOps.getTasksToBeSynced(context));
        bean.setUser(StaticOps.USER_ID);

        if(bean.getTasks() == null || bean.getTasks().size() == 0) {
            Log.d("TEST", "No tasks need sync and no tasks are sent to backEnd. Trying to retrieve tasks only.");
        } else {
            Log.d("TEST", bean.getTasks().size() + " Tasks to be synced are sent to the cloud.");
        }

        try {
            return myApiService.syncTasks(bean).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(MyBean result) {

        if(result == null || result.getTasks() == null) {
            Log.d("TEST", "No tasks were returned from the backEnd.");
            context.getOps().onEndpointAsyncTaskPostExecute(null);
            return;
        }
        Log.d("TEST", result.getTasks().size() + " Tasks were returned from the backEnd.");

        ArrayList<uom.mai.masterproject1.database.Task> frontTasks = new ArrayList<>();

        for(Task task: result.getTasks()) {

            uom.mai.masterproject1.database.Task frontTask = uom.mai.masterproject1.database.Task.getFrontEndTask(task);

            frontTasks.add(frontTask);

        }

        context.getOps().onEndpointAsyncTaskPostExecute(frontTasks);

    }

}
