package uom.mai.masterproject1.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import uom.mai.masterproject1.R;
import uom.mai.masterproject1.fragments.RetainedFragmentManager;
import uom.mai.masterproject1.operations.TaskActivityOps;

public class TaskActivity extends MyActivityType {

    private final static String TAG = "TaskActivity";

    //the fragment manager that retains the instance
    protected final RetainedFragmentManager mRetainedFragmentManager =
            new RetainedFragmentManager(this.getFragmentManager(),
                    TAG);

    private TaskActivityOps mOps;

    //setters/getters
    public TaskActivityOps getOps() { return mOps; }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setInitialUI();
        handleConfigurationChanges();

    }

    //function that setus up the UI
    private void setInitialUI(){

        setContentView(R.layout.activity_task);

    }

    @Override
    protected Dialog onCreateDialog(int id) {

        if (id == 999) {
            return mOps.onCreateDateDialog();
        }
        if(id == 888) {
            return mOps.onCreateTimeDialog();
        }
        return null;
    }

    //function that handles all the configuration changes
    protected void handleConfigurationChanges() {
        // If this method returns true then this is the first time the
        // Activity has been created.
        if (mRetainedFragmentManager.firstTimeIn()) {
            Log.d(TAG,
                    "First time onCreate() call");

            // Create the TaskActivityOps object one time.
            mOps = new TaskActivityOps(this);

            // Store the TaskActivityOps into the RetainedFragmentManager.
            mRetainedFragmentManager.put("TASK_OPS_STATE",
                    mOps);

        } else {
            Log.d(TAG,
                    "Second or subsequent onCreate() call");

            // The RetainedFragmentManager was previously initialized,
            // which means that a runtime configuration change
            // occurred, so obtain the TaskActivityOps object and inform it
            // that the runtime configuration change has completed.
            mOps =
                    mRetainedFragmentManager.get("TASK_OPS_STATE");
            mOps.onConfigurationChange(this);
        }
    }


}
