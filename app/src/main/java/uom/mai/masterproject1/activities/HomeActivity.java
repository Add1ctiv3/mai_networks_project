package uom.mai.masterproject1.activities;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import uom.mai.masterproject1.R;
import uom.mai.masterproject1.fragments.RetainedFragmentManager;
import uom.mai.masterproject1.operations.HomeActivityOps;
import uom.mai.masterproject1.operations.StaticOps;

public class HomeActivity extends MyActivityType
        implements NavigationView.OnNavigationItemSelectedListener {

    private final static String TAG = "HomeActivity";

    //the fragment manager that retains the instance
    protected final RetainedFragmentManager mRetainedFragmentManager =
            new RetainedFragmentManager(this.getFragmentManager(),
                    TAG);

    private HomeActivityOps mOps;

    //setters/getters
    public HomeActivityOps getOps() { return mOps; }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setInitialUI();
        handleConfigurationChanges();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.pastTasksButton) {
            getSupportActionBar().setTitle("Εκπρόθεσμες");
            mOps.setWindowCode(HomeActivityOps.OverdueTasksCode);
            mOps.loadOverdueTasks();
        } else if (id == R.id.todayTasksButton) {
            getSupportActionBar().setTitle("Σημερινές");
            mOps.setWindowCode(HomeActivityOps.todayTasksCode);
            mOps.loadTodayTasks();
        } else if (id == R.id.futureTasksButton) {
            getSupportActionBar().setTitle("Μελλοντικές");
            mOps.setWindowCode(HomeActivityOps.FutureTasksCode);
            mOps.loadFutureTasks();
        } else if (id == R.id.settingsButton) {

            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);

        } else if (id == R.id.completedTasksButton) {
            getSupportActionBar().setTitle("Ολοκληρωμένες");
            mOps.setWindowCode(HomeActivityOps.CompletedTasksCode);
            mOps.loadCompletedTasks();
        } else if(id == R.id.allTasksButton) {
            getSupportActionBar().setTitle("Όλες");
            mOps.setWindowCode(HomeActivityOps.allTasksCode);
            mOps.loadAllTasks();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == 1111) {

            if(resultCode == Activity.RESULT_OK) {
                String emailAddress = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                StaticOps.savePreferences(this, emailAddress);
                if(emailAddress != null && !emailAddress.equals("")) {
                    StaticOps.ID_OK = true;
                    StaticOps.USER_ID = emailAddress;
                }
            } else {
                this.finish();
            }

        } else {
            if(resultCode == Activity.RESULT_OK){
                mOps.onActivityResult(requestCode);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

    }//onActivityResult


    //function that sets up the UI
    private void setInitialUI(){

        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Σημερινές");
        setSupportActionBar(toolbar);

        FloatingActionButton mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOps.onCreateNewTaskButtonClick();
            }
        });

        DrawerLayout mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(mToggle);
        mToggle.syncState();

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        //selecting todays posts
        mNavigationView.getMenu().getItem(2).setChecked(true);

    }

    //function that handles all the configuration changes
    protected void handleConfigurationChanges() {
        // If this method returns true then this is the first time the
        // Activity has been created.
        if (mRetainedFragmentManager.firstTimeIn()) {
            Log.d(TAG,
                    "First time onCreate() call");

            // Create the HomeActivityOps object one time.
            mOps = new HomeActivityOps(this);

            // Store the HomeActivityOps into the RetainedFragmentManager.
            mRetainedFragmentManager.put("HOME_OPS_STATE",
                    mOps);

        } else {
            Log.d(TAG,
                    "Second or subsequent onCreate() call");

            // The RetainedFragmentManager was previously initialized,
            // which means that a runtime configuration change
            // occurred, so obtain the HomeActivityOps object and inform it
            // that the runtime configuration change has completed.
            mOps =
                    mRetainedFragmentManager.get("HOME_OPS_STATE");
            mOps.onConfigurationChange(this);
        }
    }

}
