package uom.mai.masterproject1.activities;

import android.os.Bundle;
import android.util.Log;
import uom.mai.masterproject1.R;
import uom.mai.masterproject1.fragments.RetainedFragmentManager;
import uom.mai.masterproject1.operations.SettingsActivityOps;

public class SettingsActivity extends MyActivityType {

    private final static String TAG = "SettingsActivity";

    //the fragment manager that retains the instance
    protected final RetainedFragmentManager mRetainedFragmentManager =
            new RetainedFragmentManager(this.getFragmentManager(),
                    TAG);

    private SettingsActivityOps mOps;
    //setters/getters
    public SettingsActivityOps getOps() { return mOps; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handleConfigurationChanges();

    }

    //function that handles all the configuration changes
    protected void handleConfigurationChanges() {
        // If this method returns true then this is the first time the
        // Activity has been created.
        if (mRetainedFragmentManager.firstTimeIn()) {
            Log.d(TAG,
                    "First time onCreate() call");

            // Create the SettingsActivityOps object one time.
            mOps = new SettingsActivityOps(this);

            // Store the TaskActivityOps into the RetainedFragmentManager.
            mRetainedFragmentManager.put("SETTINGS_OPS_STATE",
                    mOps);

        } else {
            Log.d(TAG,
                    "Second or subsequent onCreate() call");

            // The RetainedFragmentManager was previously initialized,
            // which means that a runtime configuration change
            // occurred, so obtain the SettingsActivityOps object and inform it
            // that the runtime configuration change has completed.
            mOps =
                    mRetainedFragmentManager.get("SETTINGS_OPS_STATE");
            mOps.onConfigurationChange(this);
        }
    }


}

