package uom.mai.masterproject1.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import java.util.HashMap;

public class TaskContentProvider extends ContentProvider {

    private MyDatabaseHelper dbHelper;

    private SQLiteDatabase database;

    private final static int TASKS = 100;
    private final static int TASK = 101;

    /*
    * The URI matcher used by this content provider
    * */
    public static final UriMatcher sUriMatcher =
            buildUriMatcher();

    /**
     * Helper method to match each URI to the TASK integers
     * constant defined above.
     *
     * @return UriMatcher
     */
    protected static UriMatcher buildUriMatcher() {

        // All paths added to the UriMatcher have a corresponding code
        // to return when a match is found.  The code passed into the
        // constructor represents the code to return for the rootURI.
        // It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher =
                new UriMatcher(UriMatcher.NO_MATCH);

        // For each type of URI that is added, a corresponding code is
        // created.
        matcher.addURI(TaskContract.CONTENT_AUTHORITY,
                TaskContract.PATH_DUTY,
                TASKS);
        matcher.addURI(TaskContract.CONTENT_AUTHORITY,
                TaskContract.PATH_DUTY
                        + "/#",
                TASK);
        return matcher;

    }

    //return type which represents if one item or multiple are found
    public String getType(Uri uri) {
        // Match the id returned by UriMatcher to return appropriate
        // MIME_TYPE.
        switch (sUriMatcher.match(uri)) {
            case TASKS:
                return TaskContract.TaskEntry.CONTENT_ITEMS_TYPE;
            case TASK:
                return TaskContract.TaskEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }
    }

    @Override
    public boolean onCreate() {

        Context context = getContext();
        dbHelper = new MyDatabaseHelper(context);

        database = dbHelper.getWritableDatabase();

        if(database == null) {
            return false;
        } else {
            return true;
        }
    }

    // projection map for a query
    private static HashMap<String, String> TaskMap;

    @Override
    public synchronized Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(TaskContract.TaskEntry.TABLE_NAME);

        switch (sUriMatcher.match(uri)) {
            // maps all database column names
            case TASKS:
                queryBuilder.setProjectionMap(TaskMap);
                break;
            case TASK:
                queryBuilder.appendWhere( "id" + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        Cursor cursor = queryBuilder.query(database, projection, selection,
                selectionArgs, null, null, null);

        /**
         * register to watch a content URI for changes
         */
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;

    }

    @Override
    public synchronized Uri insert(Uri uri, ContentValues values) {

        long row = database.insert(TaskContract.TaskEntry.TABLE_NAME, "", values);

        // If record is added successfully
        if(row > 0) {
            Uri newUri = ContentUris.withAppendedId(TaskContract.BASE_CONTENT_URI, row);
            getContext().getContentResolver().notifyChange(newUri, null);
            return newUri;
        }
        throw new SQLException("Fail to add a new record into " + uri);

    }

    @Override
    public int bulkInsert(Uri uri,
                          ContentValues[] cvsArray) {
        return 0;
    }

    @Override
    public synchronized int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;

        switch (sUriMatcher.match(uri)) {

            case TASKS:

                // delete all the records of the table

                count = database.delete(TaskContract.TaskEntry.TABLE_NAME, selection, selectionArgs);

                break;

            case TASK:

                String id = uri.getLastPathSegment(); //gets the id

                count = database.delete(TaskContract.TaskEntry.TABLE_NAME, "id" + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')':""),selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI " + uri);

        }

        getContext().getContentResolver().notifyChange(uri, null);

        return count;

    }

    @Override
    public synchronized int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;

        switch (sUriMatcher.match(uri)){
            case TASKS:
                count = database.update(TaskContract.TaskEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case TASK:
                count = database.update(TaskContract.TaskEntry.TABLE_NAME, values, "id" +

                        " = " + uri.getLastPathSegment() +

                        (!TextUtils.isEmpty(selection) ? " AND (" +

                                selection + ')' : ""), selectionArgs);

                break;

            default:

                throw new IllegalArgumentException("Unsupported URI " + uri );

        }

        getContext().getContentResolver().notifyChange(uri, null);

        return count;

    }


}
