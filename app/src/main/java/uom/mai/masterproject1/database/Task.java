package uom.mai.masterproject1.database;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;
import uom.mai.masterproject1.operations.StaticOps;

public class Task implements Parcelable {

    //local variables
    private long id;
    private boolean completed;
    private boolean notify;
    private String date;
    private String time;
    private String note;
    private int priority;
    private String user;
    private int updateDate;
    private boolean needsSync;
    private boolean toBeDeleted;
    private String calculatedId;

    //getters / setters
    public void setToBeDeleted(boolean toBeDeleted) {
        this.toBeDeleted = toBeDeleted;
    }

    public boolean isToBeDeleted() {

        return toBeDeleted;
    }

    public boolean isNeedsSync() {
        return needsSync;
    }

    public void setNeedsSync(boolean needsSync) {
        this.needsSync = needsSync;
    }

    public String getCalculatedId() {
        return calculatedId;
    }

    public void setCalculatedId(String id) { calculatedId = id; }

    public long getId() {
        return id;
    }

    public boolean isCompleted() {
        return completed;
    }

    public boolean doNotify() {
        return notify;
    }

    public String getDate() {
        return date;
    }

    public String getSQLDate() { return Task.getSQLDateString(date); }

    public String getTime() {
        return time;
    }

    public String getNote() {
        return note;
    }

    public int getPriority() {
        return priority;
    }

    public String getUser() {
        return user;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCompleted(boolean val) {
        completed = val;
    }

    public void setNotify(boolean val) {
        notify = val;
    }

    public void setDate(String d) {
        date = d;
    }

    public void setTime(String t) {
        time = t;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setPriority(int i) {
        priority = i;
    }

    public void setUser(String u) {
        user = u;
    }

    public void setUpdateDate(int i) {
        updateDate = i;
    }

    public Task(String notificationDate, String note) {

        id = System.currentTimeMillis()/1000;

        this.date = notificationDate;
        this.note = note;

        this.time = null;
        this.notify = false;
        this.completed = false;

        this.user = StaticOps.USER_ID;
        this.calculatedId = user + "_" + id;

        needsSync = true;

    }


    //function that return ContentValues object ready for database insertion
    public ContentValues prepareForInsert() {

        ContentValues values = new ContentValues();

        values.put(TaskContract.TaskEntry._ID, id);
        values.put(TaskContract.TaskEntry.COLUMN_DATE, getSQLDateString(date));
        values.put(TaskContract.TaskEntry.COLUMN_TIME, time);
        values.put(TaskContract.TaskEntry.COLUMN_NOTIFY, (notify?1:0));
        values.put(TaskContract.TaskEntry.COLUMN_COMPLETED, (completed?1:0));
        values.put(TaskContract.TaskEntry.COLUMN_NOTE, note);
        values.put(TaskContract.TaskEntry.COLUMN_PRIORITY, priority);
        values.put(TaskContract.TaskEntry.UPDATE_DATE, updateDate);
        values.put(TaskContract.TaskEntry.COLUMN_USER, user);
        values.put(TaskContract.TaskEntry.COLUMN_NEEDS_SYNC, (needsSync?1:0));
        values.put(TaskContract.TaskEntry.COLUMN_TO_BE_DELETED, (toBeDeleted?1:0));

        return values;

    }

    public static String getSQLDateString(String date) {

        String[] parts = date.split("/");

        String date2 = parts[2] + "-" + parts[1] + "-" + parts[0];

        return date2;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeLong(id);
        parcel.writeByte((byte) (completed ? 1 : 0));
        parcel.writeByte((byte) (notify ? 1 : 0));
        parcel.writeString(date);
        parcel.writeString(time);
        parcel.writeString(note);
        parcel.writeInt(priority);
        parcel.writeString(user);
        parcel.writeInt(updateDate);
        parcel.writeByte((byte) (needsSync ? 1 : 0));
        parcel.writeByte((byte) (toBeDeleted ? 1 : 0));

    }

    public static final Parcelable.Creator<Task> CREATOR = new Creator<Task>() {

        public Task createFromParcel(Parcel source) {

            long id = source.readLong();
            boolean completed = source.readByte() == 1 ? true : false;
            boolean notify = source.readByte() == 1 ? true : false;
            String date = source.readString();
            String time = source.readString();
            String note = source.readString();
            int priority = source.readInt();
            String user = source.readString();
            int update = source.readInt();
            boolean needsSync = source.readByte() == 1 ? true : false;
            boolean tobedeleted = source.readByte() == 1 ? true : false;

            Task mTask = new Task(date, note);
            mTask.setId(id);
            mTask.setCompleted(completed);
            mTask.setNotify(notify);
            mTask.setTime(time);
            mTask.setPriority(priority);
            mTask.setUser(user);
            mTask.setUpdateDate(update);
            mTask.setNeedsSync(needsSync);
            mTask.setToBeDeleted(tobedeleted);

            return mTask;

        }

        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public addictivelabs.mai.uom.taskEndpoint.model.Task getBackendTask() {

        addictivelabs.mai.uom.taskEndpoint.model.Task taskToRet = new addictivelabs.mai.uom.taskEndpoint.model.Task();
        taskToRet.setDate(date);
        taskToRet.setNote(note);
        taskToRet.setId(id);
        taskToRet.setCompleted(completed);
        taskToRet.setNotify(notify);
        taskToRet.setTime(time);
        taskToRet.setPriority(priority);
        taskToRet.setUser(user);
        taskToRet.setUpdateDate(updateDate);
        taskToRet.setNeedsSync(needsSync);
        taskToRet.setToBeDeleted(toBeDeleted);

        taskToRet.setCalculatedId(user + "_" + id);

        return taskToRet;

    }

    public static Task getFrontEndTask(addictivelabs.mai.uom.taskEndpoint.model.Task bTask) {

        if(bTask == null) { return null; }

        Task fTask = new Task(bTask.getDate(), bTask.getNote());
        fTask.setId(bTask.getId());
        fTask.setCompleted(bTask.getCompleted());
        fTask.setNotify(bTask.getNotify()==null?false:true);
        fTask.setTime(bTask.getTime());
        fTask.setPriority(bTask.getPriority());
        fTask.setUser(bTask.getUser());
        fTask.setUpdateDate(bTask.getUpdateDate());
        fTask.setNeedsSync(bTask.getNeedsSync());
        fTask.setToBeDeleted(bTask.getToBeDeleted());

        fTask.setCalculatedId(bTask.getCalculatedId());

        return fTask;

    }

}