package uom.mai.masterproject1.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    public final static String TAG = "TaskDatabase";

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "uom.mai.masterproject1.myAppDatabase";

    private static final String SQL_CREATE_TASKS_TABLE =
            "CREATE TABLE "
                    + TaskContract.TaskEntry.TABLE_NAME + " ("
                    + TaskContract.TaskEntry._ID + " INTEGER PRIMARY KEY,"
                    + TaskContract.TaskEntry.COLUMN_USER + " VARCHAR NOT NULL, "
                    + TaskContract.TaskEntry.COLUMN_NOTE + " TEXT NOT NULL, "
                    + TaskContract.TaskEntry.COLUMN_TIME + " TEXT, "
                    + TaskContract.TaskEntry.COLUMN_DATE + " DATE NOT NULL, "
                    + TaskContract.TaskEntry.UPDATE_DATE + " INT UNSIGNED, "
                    + TaskContract.TaskEntry.COLUMN_NOTIFY + " TINYINT NOT NULL, "
                    + TaskContract.TaskEntry.COLUMN_NEEDS_SYNC + " TINYINT NOT NULL, "
                    + TaskContract.TaskEntry.COLUMN_TO_BE_DELETED + " TINYINT NOT NULL, "
                    + TaskContract.TaskEntry.COLUMN_COMPLETED + " TINYINT NOT NULL, "
                    + TaskContract.TaskEntry.COLUMN_PRIORITY + " TINYINT UNSIGNED"

                    + " );";

    private static final String SQL_CREATE_UNIQUE_TASKS_INDEX = "CREATE UNIQUE INDEX tasksIndex ON " +
            TaskContract.TaskEntry.TABLE_NAME + "("+TaskContract.TaskEntry._ID+")";

    private static final String SQL_DROP_TASKS_INDEX = "DROP INDEX tasksIndex;";

    private static final String SQL_DELETE_TASKS_TABLE =
            "DROP TABLE IF EXISTS " + TaskContract.TaskEntry.TABLE_NAME;

    //constructor
    public MyDatabaseHelper(Context context) {
        super(context,
                context.getCacheDir()
                        + File.separator
                        + DATABASE_NAME,
                null,
                DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create the table
        db.execSQL(SQL_CREATE_TASKS_TABLE);
        db.execSQL(SQL_CREATE_UNIQUE_TASKS_INDEX);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //Delete the existing tables
        db.execSQL(SQL_DELETE_TASKS_TABLE);
        db.execSQL(SQL_DROP_TASKS_INDEX);
        //create the new tables
        onCreate(db);
    }

}
