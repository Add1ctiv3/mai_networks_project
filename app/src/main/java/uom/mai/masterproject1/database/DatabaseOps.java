package uom.mai.masterproject1.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DatabaseOps {

    private final static String TAG = "TESTING";

    private static void logAction(Task task, String actionType) {

        Log.d(TAG, "=========================================");

        Log.d(TAG, actionType);

        if(task != null) {
            Log.d(TAG, "TASK:");
            Log.d(TAG, "id: " + task.getCalculatedId());
            Log.d(TAG, "note: " + task.getNote());
            Log.d(TAG, "date: " + task.getDate());
            Log.d(TAG, "time: " + task.getTime());
            Log.d(TAG, "need to be synced: " + task.isNeedsSync());
            Log.d(TAG, "is completed: " + task.isCompleted());
            Log.d(TAG, "is to be deleted: " + task.isToBeDeleted());
            Log.d(TAG, "update date int: " + task.getUpdateDate());
        }

        Log.d(TAG, "=========================================");

    }

    public static boolean insertTask(Task task, Context context) {

        if(task == null) { return false; }

        //logAction(task, "Preparing to insert task.");

        task.setUpdateDate((int)System.currentTimeMillis()/1000);

        ContentValues values = task.prepareForInsert();

        Uri uri = context.getContentResolver().insert(TaskContract.BASE_CONTENT_URI, values);

        if(uri != null) {
            logAction(task, "Task inserted.");
        }

        return uri != null;

    }

    public static boolean updateTask(Task task, Context context) {

        if( task == null ) { return false; }

        //logAction(task, "Preparing to update task.");

        task.setUpdateDate((int)System.currentTimeMillis()/1000);

        ContentValues values = task.prepareForInsert();

        String whereClause = TaskContract.TaskEntry._ID + "=" + task.getId();

        context.getContentResolver().update(TaskContract.TaskEntry.CONTENT_URI, values, whereClause, null);

        logAction(task, "Task updated.");

        return true;

    }

    public static Task getTask(long id, Context context) {

        String whereClause = TaskContract.TaskEntry._ID + "=" + id;

        ArrayList<Task> list = DatabaseOps.returnTasks(whereClause, context);

        if(list != null && list.size() > 0) {
            return list.get(0);
        }

        return null;

    }

    public static boolean taskExists(Task task, Context context) {

        String whereClause = TaskContract.TaskEntry.COLUMN_DATE + "='" + task.getSQLDate() + "' AND " + TaskContract.TaskEntry.COLUMN_NOTE + "='" + task.getNote() + "' AND " + TaskContract.TaskEntry.COLUMN_COMPLETED + " =0 ";

        Cursor cursor = context.getContentResolver().query(TaskContract.TaskEntry.CONTENT_URI, null, whereClause, null, null);

        if(cursor != null && cursor.getCount() == 0) {
            //Log.d(TAG, "taskExists() returned false.");
            cursor.close();
            return false;
        } else {
            //Log.d(TAG, "taskExists() returned true.");
            if(cursor != null) {
                cursor.close();
            }
            return true;
        }

    }

    public static boolean taskWithThisIdExists(Task task, Context context) {

        String whereClause = TaskContract.TaskEntry._ID + "=" + task.getId();

        Cursor cursor = context.getContentResolver().query(TaskContract.TaskEntry.CONTENT_URI, null, whereClause, null, null);

        if(cursor != null && cursor.getCount() == 0) {
            Log.d(TAG, "taskWithThisIdExists() returned false.");
            cursor.close();
            return false;
        } else {
            Log.d(TAG, "taskWithThisIdExists() returned true.");
            if(cursor != null) {
                cursor.close();
            }
            return true;
        }

    }

    private static ArrayList<Task> returnTasks(String whereClause, Context context) {

        ArrayList<Task> retTasks = new ArrayList<>();

        Cursor cursor = context.getContentResolver().query(TaskContract.TaskEntry.CONTENT_URI, null, whereClause, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {

                long id = cursor.getInt(cursor.getColumnIndex(TaskContract.TaskEntry._ID));
                String note = cursor.getString(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NOTE));
                String date = cursor.getString(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_DATE));
                String time = cursor.getString(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_TIME));
                boolean completed = cursor.getInt(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_COMPLETED)) == 1;
                boolean notify = cursor.getInt(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NOTIFY)) == 1;
                int priority = cursor.getInt(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_PRIORITY));
                int update_date = cursor.getInt(cursor.getColumnIndex(TaskContract.TaskEntry.UPDATE_DATE));
                String user = cursor.getString(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_USER));
                boolean needsSync = cursor.getInt(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NEEDS_SYNC)) == 1;
                boolean toBeDeleted = cursor.getInt(cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_TO_BE_DELETED)) == 1;

                Task returnTask = new Task(getNormalDate(date), note);
                returnTask.setId(id);
                returnTask.setTime(time);
                returnTask.setCompleted(completed);
                returnTask.setNotify(notify);
                returnTask.setPriority(priority);
                returnTask.setUpdateDate(update_date);
                returnTask.setUser(user);
                returnTask.setNeedsSync(needsSync);
                returnTask.setToBeDeleted(toBeDeleted);

                retTasks.add(returnTask);

            } while (cursor.moveToNext());

        } else
        {
            if(cursor != null) { cursor.close(); }
            return null;
        }

        cursor.close();

        return retTasks;

    }

    public static ArrayList<Task> getTodayTasks(Context context) {

        Calendar cal = Calendar.getInstance();
        String targetDate = getSQLDateString(cal.getTime());

        String whereClause = TaskContract.TaskEntry.COLUMN_DATE + "<='" + targetDate + "' AND " + TaskContract.TaskEntry.COLUMN_COMPLETED + "=0 AND " + TaskContract.TaskEntry.COLUMN_TO_BE_DELETED + "=0" ;

        return returnTasks(whereClause, context);

    }

    public static ArrayList<Task> getOverdueTasks(Context context) {

        Calendar cal = Calendar.getInstance();
        String targetDate = getSQLDateString(cal.getTime());

        String whereClause = TaskContract.TaskEntry.COLUMN_DATE + "<'" + targetDate + "' AND " + TaskContract.TaskEntry.COLUMN_COMPLETED + "=0 AND " + TaskContract.TaskEntry.COLUMN_TO_BE_DELETED + "=0" ;

        return returnTasks(whereClause, context);

    }

    public static ArrayList<Task> getFutureTasks(Context context) {

        Calendar cal = Calendar.getInstance();
        String targetDate = getSQLDateString(cal.getTime());

        String whereClause = TaskContract.TaskEntry.COLUMN_DATE + ">'" + targetDate + "' AND " + TaskContract.TaskEntry.COLUMN_COMPLETED + "=0 AND " + TaskContract.TaskEntry.COLUMN_TO_BE_DELETED + "=0" ;

        return returnTasks(whereClause, context);

    }

    public static ArrayList<Task> getAllTasks(Context context) {

        String whereClause = TaskContract.TaskEntry.COLUMN_COMPLETED + "=0 AND " + TaskContract.TaskEntry.COLUMN_TO_BE_DELETED + "=0" ;

        return returnTasks(whereClause, context);

    }

    public static ArrayList<Task> getCompletedTasks(Context context) {

        String whereClause = TaskContract.TaskEntry.COLUMN_COMPLETED + "=1 AND " + TaskContract.TaskEntry.COLUMN_TO_BE_DELETED + "=0";

        return returnTasks(whereClause, context);

    }

    public static Task getTaskByNoteAndDate(String note, String date, Context context) throws ParseException {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = sdf.parse(date);

        String whereClause = TaskContract.TaskEntry.COLUMN_NOTE + "='" + note + "' AND " + TaskContract.TaskEntry.COLUMN_DATE + "='"
                + getSQLDateString(d) + "' AND " + TaskContract.TaskEntry.COLUMN_TO_BE_DELETED + "=0" ;

        ArrayList<Task> list = returnTasks(whereClause, context);

        if(list != null && list.size()>0) {
            return list.get(0);
        }

        return null;

    }

    public static ArrayList<addictivelabs.mai.uom.taskEndpoint.model.Task> getTasksToBeSynced(Context context) {

        String whereClause = TaskContract.TaskEntry.COLUMN_NEEDS_SYNC + "=1";

        ArrayList<Task> t = returnTasks(whereClause, context);

        ArrayList<addictivelabs.mai.uom.taskEndpoint.model.Task> tasks = new ArrayList<>();

        if(t == null || t.size() == 0) { return null; }

        for(Task task: t) {
            tasks.add(task.getBackendTask());
        }

        return tasks;

    }

    public static void deleteSyncedTasksToBeDeleted(Context context) {

        logAction(null, "Deleting all local tasks that are now synced and need to be deleted.");

        String whereClause = TaskContract.TaskEntry.COLUMN_TO_BE_DELETED + "=1";

        context.getContentResolver().delete(TaskContract.TaskEntry.CONTENT_URI, whereClause, null);

    }

    private static String getNormalDate(String sqlDate) {

        String[] parts = sqlDate.split("-");
        return parts[2] + "/" + parts[1] + "/" + parts[0];

    }

    //return the date in sql format
    private static String getSQLDateString(Date date) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

}
