package uom.mai.masterproject1.database;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class TaskContract {

    /*
    * This Content Providers unique identifier
    */
    public static final String CONTENT_AUTHORITY =
            "uom.mai.masterproject1.taskprovider";

    /*
     * Use Content authority to create the base of all URI's which
     * apps will use to contact the content provider
     */
    public static final Uri BASE_CONTENT_URI =
            Uri.parse("content://"
                        + CONTENT_AUTHORITY);

    /**
     * Possible paths (appended to base content URI for possible
     * URI's).  For instance, content://vandy.mooc/character_table/ is
     * a valid path for looking at Character data.  Conversely,
     * content://vandy.mooc/givemeroot/ will fail, as the
     * ContentProvider hasn't been given any information on what to do
     * with "givemeroot".
     */
    public static final String PATH_DUTY =
            TaskEntry.TABLE_NAME;


    public static final class TaskEntry implements BaseColumns {
        /**
         * Use BASE_CONTENT_URI to create the unique URI for tasks
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_DUTY).build();

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_DUTY;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_DUTY;

        /**
         * Columns to display.
         */
        public static final String sColumnsToDisplay [] =
                new String[] {
                        TaskEntry._ID,
                        TaskEntry.COLUMN_DATE,
                        TaskEntry.COLUMN_TIME,
                        TaskEntry.COLUMN_PRIORITY,
                        TaskEntry.COLUMN_NOTE,
                        TaskEntry.COLUMN_NOTIFY,
                        TaskEntry.COLUMN_COMPLETED,
                        TaskEntry.UPDATE_DATE,
                        TaskEntry.COLUMN_USER,
                        TaskEntry.COLUMN_NEEDS_SYNC,
                        TaskEntry.COLUMN_TO_BE_DELETED
                };

         /**
         * Name of the database table.
         */
        public static final String TABLE_NAME =
                "tasks_table";

        /**
         * Columns to store data.
         */
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_TIME = "time";
        public static final String COLUMN_PRIORITY = "priority";
        public static final String COLUMN_NOTE = "note";
        public static final String COLUMN_NOTIFY = "notify";
        public static final String COLUMN_COMPLETED = "completed";
        public static final String UPDATE_DATE = "update_date";
        public static final String COLUMN_USER = "user_email";
        public static final String COLUMN_NEEDS_SYNC = "needs_sync";
        public static final String COLUMN_TO_BE_DELETED = "to_be_deleted";

        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id
         * @return Uri
         */
        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI,
                    id);
        }
    }

}
