package uom.mai.masterproject1.backend;

import java.util.ArrayList;

public class MyBean {

    private ArrayList<Task> tasks;
    private String user;

    public String getUser() { return user; }
    public void setUser(String u) { user = u; }

    public ArrayList<Task> getTasks() { return tasks; }
    public void setTasks(ArrayList<Task> list) { tasks = list; }

    public MyBean(ArrayList<Task> list) {
        tasks = list;
    }

    public MyBean() {}

}
