package uom.mai.masterproject1.backend;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.Nullable;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.cmd.Query;
import java.util.ArrayList;
import java.util.List;
import static com.googlecode.objectify.ObjectifyService.ofy;

@Api(name = "taskEndpoint", version = "v1",
        namespace = @ApiNamespace(ownerDomain = "uom.mai.addictivelabs",
                ownerName = "uom.mai.addictivelabs", packagePath=""))

public class TaskEndpoint {

    public TaskEndpoint() {

    }

    /**
     * Return a collection of all the tasks
     * @param //a collection of tasks to be synced
     * @return a list of Tasks
     */
    @ApiMethod(name="syncTasks", httpMethod = ApiMethod.HttpMethod.POST, path="SynchronizeTasks")
    public MyBean syncTasks(MyBean tasksToBeSynced) {

        if(tasksToBeSynced.getTasks() != null && tasksToBeSynced.getTasks().size() > 0) {

            System.out.println("=============================================================");

            System.out.println("Got a list with " + tasksToBeSynced.getTasks().size() + " tasks.");

            for (Task task : tasksToBeSynced.getTasks()) {

                System.out.println("-------------------------------------------");
                System.out.println("Task ID: " + task.getCalculated_id());
                System.out.println("Task Date: " + task.getDate());
                System.out.println("Task Note: " + task.getNote());

                Task storedTask = findRecord(task.getCalculated_id());

                if (storedTask != null) {
                    if (storedTask.getUpdateDate() < task.getUpdateDate()) {
                        try {
                            if (task.isToBeDeleted()) {
                                System.out.println("Task to)be_deleted is TRUE! Removing Task...");
                                removeTask(storedTask.getCalculated_id());
                                System.out.println("Task was removed.");
                            } else {
                                System.out.println("Task needsSync is TRUE! Updating Task...");
                                task.setNeedsSync(false);
                                updateTask(task);
                                System.out.println("Task was updated.");
                            }
                        } catch (NotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("Task exists and doesn't need update!");
                    }
                } else {
                    if (!task.isToBeDeleted()) {
                        try {
                            System.out.println("Task doesn't exist! Inserting Task...");
                            task.setNeedsSync(false);
                            insertTask(task);
                            System.out.println("Task was inserted.");
                        } catch (ConflictException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.out.println("-------------------------------------------");

            } //end of updating backend database

        }

        System.out.println("");

        //return listOfTasks
        System.out.println("Gathering the Tasks to return from user " + tasksToBeSynced.getUser());
        ArrayList<Task> list = new ArrayList<>();
        for(Task task: listTask(null, null, tasksToBeSynced.getUser()).getItems()) {
            list.add(task);
        }
        System.out.println("Gathered "+list.size()+" Tasks. Returning the list...");
        MyBean response = new MyBean(list);
        System.out.println("=============================================================");
        return response;

    }

    /**
     * Return a collection of tasks
     * @param count The number of tasks
     * @return a list of Tasks
     */
    @ApiMethod(name = "listTask")
    public CollectionResponse<Task> listTask(@Nullable @Named("cursor") String cursorString,
                                              @Nullable @Named("count") Integer count,
                                              @Named("queryPart") String user) {

        Query<Task> query = ofy().load().type(Task.class).filter(" user = ", user);

        if (count != null) query.limit(count);

        if (cursorString != null && cursorString != "") {
            query = query.startAt(Cursor.fromWebSafeString(cursorString));
        }

        List<Task> records = new ArrayList<Task>();

        QueryResultIterator<Task> iterator = query.iterator();
        int num = 0;

        while (iterator.hasNext()) {
            records.add(iterator.next());
            if (count != null) {
                num++;
                if (num == count) break;
            }
        }

        //Find the next cursor
        if (cursorString != null && cursorString != "") {
            Cursor cursor = iterator.getCursor();
            if (cursor != null) {
                cursorString = cursor.toWebSafeString();
            }
        }

        return CollectionResponse.<Task>builder().setItems(records).setNextPageToken(cursorString).build();

    }

    /**
     * This inserts a new <code>Task</code> object.
     * @param task The object to be added.
     * @return The object to be added.
     */
    @ApiMethod(name = "insertTask")
    public Task insertTask(Task task) throws ConflictException {
        //If if is not null, then check if it exists. If yes, throw an Exception
        //that it is already present
        if (task.getCalculated_id() != null) {
            if (findRecord(task.getCalculated_id()) != null) {
                throw new ConflictException("Object already exists");
            }
        }
        //Since our @Id field is a Long, Objectify will generate a unique value for us
        //when we use put
        OfyService.ofy().save().entity(task).now();
        return task;
    }

    /**
     * This updates an existing <code>Task</code> object.
     * @param task The object to be added.
     * @return The object to be updated.
     */
    @ApiMethod(name = "updateTask")
    public Task updateTask(Task task)throws NotFoundException {
        if (findRecord(task.getCalculated_id()) == null) {
            throw new NotFoundException("Task Record does not exist");
        }
        OfyService.ofy().save().entity(task).now();
        return task;
    }

    /**
     * This deletes an existing <code>Task</code> object.
     * @param id The id of the object to be deleted.
     */
    @ApiMethod(name = "removeTask")
    public void removeTask(@Named("id") String id) throws NotFoundException {
        Task record = findRecord(id);
        if(record == null) {
            throw new NotFoundException("Task Record does not exist");
        }
        OfyService.ofy().delete().entity(record).now();
    }

    //Private method to retrieve a <code>Task</code> record
    private Task findRecord(String id) {
        return OfyService.ofy().load().type(Task.class).id(id).now();
    }

}
