package uom.mai.masterproject1.backend;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class Task {

    //local variables
    private long id;
    private boolean completed;
    private boolean notify;
    private String date;
    private String time;
    private String note;
    @Id
    private String calculatedId;

    private int priority;

    @Index
    private String user;
    private boolean needsSync;
    private int updateDate;
    private boolean toBeDeleted;

    //getters / setters
    public void setToBeDeleted(boolean toBeDeleted) {
        this.toBeDeleted = toBeDeleted;
    }

    public boolean isToBeDeleted() {

        return toBeDeleted;
    }

    public long getId() {
        return id;
    }

    public boolean isCompleted() {
        return completed;
    }

    public boolean isNeedsSync() {
        return needsSync;
    }

    public void setNeedsSync(boolean needsSync) {
        this.needsSync = needsSync;
    }


    public String getCalculated_id() {
        return calculatedId;
    }

    public void setCalculated_id(String id) { calculatedId = id; }

    public boolean doNotify() {
        return notify;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getNote() {
        return note;
    }

    public int getPriority() {
        return priority;
    }

    public String getUser() {
        return user;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCompleted(boolean val) {
        completed = val;
    }

    public void setNotify(boolean val) {
        notify = val;
    }

    public void setDate(String d) {
        date = d;
    }

    public void setTime(String t) {
        time = t;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setPriority(int i) {
        priority = i;
    }

    public void setUser(String u) {
        user = u;
    }

    public void setUpdateDate(int i) {
        updateDate = i;
    }

    public Task(String notificationDate, String note) {

    }

    public Task() {}

}