This repository contains an android app that is created in order to demonstrate an easy way to provide communication between your app and the Google Cloud.

It is created with Android Studio.

After you download and import the project you simply run the backend module and then the android app using an emulator. If you want to use a real device make sure to change the ip address in EndpointsAsyncTask ( .setRootUrl("http://10.0.2.2:8080/_ah/api/") ) to the corresponding address of your "server". You can use ipconfig command in a command line to check your machines ip.